
import os
import json
import tensorflow as tf
import numpy as np
import cv2
import re













def get_cedula_images_2(model_ced_path,image,MIN_CON,detection_graph,sess):
    imr=None
    imp=image.copy()
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')
    detection_boxes = detection_graph.get_tensor_by_name('detection_boxes:0')
    detection_scores = detection_graph.get_tensor_by_name('detection_scores:0')
    detection_classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')
    (im_width, im_height,ch) = image.shape
    image_np = np.array(image)
    image_np_expanded = np.expand_dims(image_np, axis=0)
    (boxes, scores, classes, num) = sess.run([detection_boxes, detection_scores, detection_classes, num_detections], feed_dict={image_tensor: image_np_expanded})
    rects=[]
    rectsf=[]
    faces_en=[]
    factor=4
    delta = 1 / factor
    imo=imp.copy()
    cer=np.zeros_like(imo)
    for k,score in enumerate(scores[0]):
        if(score>=MIN_CON):
            if(classes[0][k]==1 or classes[0][k]==4 or classes[0][k]==6 or classes[0][k]==5 or classes[0][k]==7 or classes[0][k]==8):
                YMIN = a0 = boxes[0][k][0] * im_height
                YMAX = a2 = boxes[0][k][2] * im_height
                XMIN = a1 = boxes[0][k][1] * im_width
                XMAX = a3 = boxes[0][k][3] * im_width
                #rect = [int(a1/factor), int(a0/factor),int(a3/factor),int(a2/factor)]
                #rects.append(rect)
                Roi = imp[int(YMIN):int(YMAX), int(XMIN):int(XMAX)]

                faces_en.append(Roi.copy())
                #rectsf.append([int(0*delta),int(0*delta),int(Roi.shape[0]*delta),int(Roi.shape[1]*delta)])
                Roi = [int(YMIN),int(YMAX), int(XMIN),int(XMAX)]
                rects.append([int(XMIN),int(YMIN),int(XMAX),int(YMAX)])
                rectsf.append(int(classes[0][k]))
                color=(255,255,255)
                cv2.rectangle(cer,(Roi[2],Roi[0]),(Roi[3],Roi[1]),color,-1)
    return imp,rects,rectsf,imo,faces_en,(cer[:,:,0]/255).astype(np.uint8)





# In[0]


# In[1]

from word_detector.sl_model import DSODSL512

from ssd.ssd_utils import load_weights

model = DSODSL512()
#model = DSODSL512(activation='leaky_relu')
weights_path = "../models/weights.012.h5"
freeze = []
batch_size = 6
experiment = 'dsodsl512_synthtext'


if weights_path is not None:
    load_weights(model, weights_path)


print(model.summary())

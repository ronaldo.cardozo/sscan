from ROI_selector.crnn_model import CRNN
from ROI_selector.crnn_utils import alphabet87 as alphabet
from ROI_selector.crnn_data import crop_words
from ROI_selector.crnn_data import joiner
from ROI_selector.crnn_data import extend_polygons
from ROI_selector.crnn_utils import decode

input_wg,input_hg=384,32
weights_path=""
Model=[]
def init(model_path=weights_path,input_w=input_wg,input_h=input_hg):
    global weights_path,Model,input_wg,input_hg
    weights_path=model_path;input_wg=input_w;input_hg=input_h
    Model = CRNN((input_wg, input_hg, 1), len(alphabet), prediction_only=True, gru=True)
    Model.load_weights(weights_path, by_name=True)

def get_words_obo(words):
    global Model
    return Model.predict(words)



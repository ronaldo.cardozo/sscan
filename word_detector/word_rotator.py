from word_detector.sl_model import DSODSL512
from word_detector.sl_utils import PriorUtil
from ssd.ssd_data import preprocess
from word_detector.sl_utils import rbox_to_polygon
from ROI_selector.crnn_data import joiner
from ROI_selector.crnn_data import extend_polygons
import tensorflow as tf
import numpy as np

sl_graph = tf.Graph();sl_session=[];prior_util=[];
sl_model = []
Model=[]
input_shape = (512, 512, 3)
weights_path = 'models/model_rot.h5'
segment_threshold = 0.55
link_threshold = 0.30;
def init(model_path=weights_path,segment_th=segment_threshold,link_th=link_threshold,input_shap=input_shape):
    global sl_model,input_shape,weights_path,segment_threshold,link_threshold,sl_session,prior_util,Model
    Model = DSODSL512
    weights_path=model_path;segment_threshold=segment_th;link_threshold=link_th;input_shape=input_shap;
    sl_model = Model(input_shape)
    prior_util = PriorUtil(sl_model)
    sl_model.load_weights(weights_path, by_name=True)

def get_words_rotated(img,input_size):
    global sl_session,sl_model,prior_util
    x = np.array([preprocess(img, input_size)])
    y = sl_model.predict(x)
    result = prior_util.decode(y[0], segment_threshold, link_threshold)
    return result

def process_rboxes(result):
    rboxes = result[:, :5]
    if len(rboxes) > 0:
        bh = rboxes[:, 3]
        rboxes[:, 2] += bh * 0.1
        rboxes[:, 3] += bh * 0.2

        boxes = np.asarray([rbox_to_polygon(r) for r in rboxes])
        boxes = np.flip(boxes, axis=1)
        boxes = np.reshape(boxes, (-1, 8))

        boxes_mask = np.array(
            [not (np.any(b < 0 - 10) or np.any(b > 512 + 10)) for b in boxes])

        boxes = boxes[boxes_mask]
        rboxes = rboxes[boxes_mask]

        if len(boxes) == 0:
            boxes = np.empty((0, 8))

        boxes = np.clip(boxes / 512, 0, 1)
        #boxes=extend_polygons(1.05,boxes.copy())
        polyss = joiner(boxes)

        boxess = []
        for poly in polyss:
            bb = np.array(poly).reshape(1, 8)
            bbx = bb[0][0::2].copy()
            bby = bb[0][1::2].copy()
            indexx = np.argsort(bbx)
            roll = indexx[0]
            if (bby[indexx[0]] > bby[indexx[1]]): roll = indexx[1]
            bb = np.roll(bb.copy(), roll * 2)
            boxess.append(bb)
        boxess = np.array(boxess).reshape(-1, 8)
        boxes = boxess.copy()
        return boxes,rboxes,True
    return 0,0,False
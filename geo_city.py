import requests
import pandas
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
import time


city_list=[]
city_name=[]
def init(ciudades):
    for ciudad in ciudades:
        path="https://nominatim.openstreetmap.org/search.php?q={}+Central+Paraguay&polygon_geojson=1&format=json".format(ciudad.replace(" ","+"))
        r = requests.get(path)
        dict = r.json()
        ciudad_p= dict[0]['geojson']['coordinates'][0][0::2]
        city_list.append(Polygon(ciudad_p))
        city_name.append(ciudad)
        

def get_city(point):
    for city_p,city_n in zip(city_list,city_name):
        
polygon = Polygon(sanlorenzo)



def is_san_lorenzo(pos):
    global polygon
    point=Point(pos[0],pos[1])
    return polygon.contains(point)




df=pandas.read_csv("./geo_data.csv")
start=time.time()
df['san_lo']=df[['O_longitude','O_latitude']].apply(is_san_lorenzo,axis=1)
print("Tiempo {}".format(time.time()-start))
df.to_csv("san_lorenzo.csv",index=False)
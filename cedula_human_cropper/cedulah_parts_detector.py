import tensorflow as tf
import numpy as np
import cv2


model_p = "./model_body.pb"
min_threshold = 0.5
model = tf.Graph()
sess=[]

def init(min_score=min_threshold,path_m="model_body.pb"):
    global model_p,min_threshold,model,sess
    min_threshold=min_score;model_p=path_m;
    with model.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(model_p, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')
    sess = tf.Session(graph=model)









def get_parts_cropped(image):
    global model,min_threshold,sess
    frame = image.copy()[:,:,::-1]
    faces, rects, classes, ima, rois, maskc = _get_parts_cropped_(frame, min_threshold, model, sess)
    return faces, rects, classes, ima, rois, maskc



def _get_parts_cropped_(image, MIN_CON, model, sess):
    imr=None
    imp=image.copy()
    image_tensor = model.get_tensor_by_name('image_tensor:0')
    detection_boxes =model.get_tensor_by_name('detection_boxes:0')
    detection_scores = model.get_tensor_by_name('detection_scores:0')
    detection_classes = model.get_tensor_by_name('detection_classes:0')
    num_detections = model.get_tensor_by_name('num_detections:0')
    (im_width, im_height,ch) = image.shape
    image_np = np.array(image)
    image_np_expanded = np.expand_dims(image_np, axis=0)
    (boxes, scores, classes, num) = sess.run([detection_boxes, detection_scores, detection_classes, num_detections], feed_dict={image_tensor: image_np_expanded})
    rects=[]
    rectsf=[]
    faces_en=[]
    imo=imp.copy()
    cer=np.zeros_like(imo)
    for k,score in enumerate(scores[0]):
        if(score>=MIN_CON):
            if(classes[0][k] in [3,4,5,6,7,8]):
                YMIN = a0 = boxes[0][k][0] * im_height
                YMAX = a2 = boxes[0][k][2] * im_height
                XMIN = a1 = boxes[0][k][1] * im_width
                XMAX = a3 = boxes[0][k][3] * im_width
                #rect = [int(a1/factor), int(a0/factor),int(a3/factor),int(a2/factor)]
                #rects.append(rect)
                Roi = imp[int(YMIN):int(YMAX), int(XMIN):int(XMAX)]

                faces_en.append(Roi.copy())
                #rectsf.append([int(0*delta),int(0*delta),int(Roi.shape[0]*delta),int(Roi.shape[1]*delta)])
                Roi = [int(YMIN),int(YMAX), int(XMIN),int(XMAX)]
                rects.append([int(XMIN),int(YMIN),int(XMAX),int(YMAX)])
                rectsf.append(int(classes[0][k]))
                color=(255,255,255)
                cv2.rectangle(cer,(Roi[2],Roi[0]),(Roi[3],Roi[1]),color,-1)
    return imp,rects,rectsf,imo,faces_en,(cer[:,:,0]/255).astype(np.uint8)


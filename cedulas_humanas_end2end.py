import numpy as np
import cv2
import tensorflow as tf
import warnings
import time

warnings.filterwarnings("ignore")

from word_detector.sl_model import DSODSL512
from word_detector.sl_utils import PriorUtil
from ssd.ssd_data import preprocess
from word_detector.sl_utils import rbox_to_polygon

from ROI_selector.crnn_model import CRNN
from ROI_selector.crnn_utils import alphabet87 as alphabet
from ROI_selector.crnn_data import crop_words
from ROI_selector.crnn_data import joiner
from ROI_selector.crnn_data import extend_polygons
from ROI_selector.crnn_utils import decode
import word_detector.word_predictor as word_predictor
import cedula_human_cropper.cedulah_parts_detector as cedula_1_model
import word_detector.word_rotator as word_rotator
import glob

# Un mensaje introductorio, que explique la funcion del script
# Explicar que parametros usa, para facilitar a la hora de modularizar
# Variables que seria bueno explicar: rboxes ; polyss ; bb
# Funciones que seria bueno explicar: cv2.bitwise_and ; rbox_to_polygon ; crop_words
#   En general las funciones exportadas tambien seria bueno explicar
# Las variables tambien estarian bien explicar, y mejor si define al principio

model_cedula = "./models/cedula_human_todo_v1.pb"
min_threshold = 0.94
input1_shape = (700, 700)
# model_Word_rotator parameters
input_size = (512, 512)
input_shape=(512,512,3)
weights_path = './models/model_rot.h5'
segment_threshold = 0.55
link_threshold = 0.30
#word detector parameters
weights_path_md = 'models/word_detector.h5'
input_width = 384
input_height = 32


if __name__ == '__main__':

    cedula_1_model.init(min_threshold, model_cedula)
    word_rotator.init(weights_path,segment_threshold,link_threshold)
    word_predictor.init(weights_path_md,input_width,input_height)

    files = glob.glob("./test/*.jpg")
    ik = 0;


    while ik < len(files):
        start = time.time()
        print("Processing files {0}".format(files[ik]))
        img = cv2.imread(files[ik]);image = img.copy()
        
        faces, rects, classes, ima, rois, maskc = cedula_1_model.get_parts_cropped(image)
        print("Etapa 1 {}".format(time.time()-start))
        img = cv2.bitwise_and(img, img, mask=maskc)
        print("Existen {0} classes".format(len(classes)));ik += 1
        result=word_rotator.get_words_rotated(img,input_size)
        boxes,rboxes,it_works=word_rotator.process_rboxes(result)
        print("Etapa 2 {}".format(time.time()-start))
        img1 = np.copy(img)
        img2 = np.zeros_like(img)

        if(it_works):
            words, centersx, centersy = crop_words(img, boxes, input_height, width=input_width, grayscale=True)
            words = np.asarray([w.transpose(1, 0, 2) for w in words])
            Z = [x for _, x in sorted(zip(centersy, words))]
            if len(words) > 0:
                res_crnn=word_predictor.get_words_obo(words)

            words = Z.copy()
            for i in range(len(words)):
                idxs = np.argmax(res_crnn[i], axis=1)
                confs = res_crnn[i][range(len(idxs)), idxs]
                non_blank_mask = idxs != len(alphabet) - 1

                if np.any(non_blank_mask):
                    mean_conf = np.mean(confs[non_blank_mask])
                    chars = [alphabet[c] for c in idxs]
                    res_str = decode(chars)
                    # filter based on recognition threshold
                    # if mean_conf > 0.7-0.4*np.exp(-0.1*np.sum(non_blank_mask)):
                    if mean_conf > 0.5:
                        b = boxes[i].reshape((-1, 1, 2)) * [img.shape[0], img.shape[1]]
                        b = np.asarray(np.round(b), dtype=np.int32)
                        cv2.polylines(img1, [b], True, (0, 0, 255))
                        res_str = res_str.upper()
                        # print("Y location: {0}".format(boxes))
                        if (res_str.count('-') == 2):
                            print("Nacimiento en fecha: {0}".format(res_str))
                        elif (res_str.isdecimal() and len(res_str) > 5):
                            print("Cedula de identidad es: {0}".format(res_str))
                        else:
                            print("Extra data: {0}".format(res_str))

                        # cv2.imwrite('croped_word_%03i.png' % (i), words[i])
                        # print(res_str)
                    else:
                        # print('drop %5.3f %s' % (mean_conf, res_str))
                        pass

        print(time.time()-start)

# ffmpeg -y -i sl_end2end_record.avi -c:v libx264 -b:v 2400k -preset slow -movflags +faststart -pix_fmt yuv420p sl_end2end_record.mp4

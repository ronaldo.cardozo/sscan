import numpy as np
import cv2
import tensorflow as tf

from word_detector.sl_model import DSODSL512
from word_detector.sl_utils import PriorUtil
from ssd.ssd_data import preprocess
from word_detector.sl_utils import rbox_to_polygon

from ROI_selector.crnn_model import CRNN
from ROI_selector.crnn_utils import alphabet87 as alphabet
from ROI_selector.crnn_data import crop_words, extend_polygons
from ROI_selector.crnn_data import joiner
from ROI_selector.crnn_utils import decode
from cedula_human_cropper.cedula_verde_cropper import get_cedula_images_2
import glob

if __name__ == '__main__':
    model_cedula = "./models/cedula_verde_features.pb"
    min_threshold = 0.94
    input1_shape = (700, 700)
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(model_cedula, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')
    
    Model = DSODSL512
    input_shape = (512,512,3)
    weights_path = '../models/model_rot.h5'
    segment_threshold = 0.55
    link_threshold = 0.50
    
    sl_graph = tf.Graph()
    with sl_graph.as_default():
        sl_session = tf.Session()
        with sl_session.as_default():
            sl_model = Model(input_shape)
            prior_util = PriorUtil(sl_model)
            sl_model.load_weights(weights_path, by_name=True)

    input_width = 384
    input_height = 32
    weights_path = '../models/word_detector.h5'
    
    crnn_graph = tf.Graph()
    with crnn_graph.as_default():
        crnn_session = tf.Session()
        with crnn_session.as_default():
            crnn_model = CRNN((input_width, input_height, 1), len(alphabet), prediction_only=True, gru=True)
            crnn_model.load_weights(weights_path, by_name=True)

    input_size=(512,512)
    if (True):
        files=glob.glob("./test_verde/*.jpg")
        ik=0;
        print(len(files))


        while ik<len(files):
            print("Processing files {0}".format(files[ik]))

            img = cv2.imread(files[ik])
            image=img.copy()

            with tf.Session(graph=detection_graph) as sess:
                    frame = cv2.resize(img[:, :, :], (700, 700))
                    faces, rects, classes, ima, rois,maskc = get_cedula_images_2(model_cedula, frame, min_threshold,
                                                                                    detection_graph, sess)
            img=cv2.bitwise_and(img, img, mask=maskc)
            print("Existen {0} classes".format(len(classes)))
            ik+=1
            x = np.array([preprocess(img, input_size)])
            with sl_graph.as_default():
                with sl_session.as_default():
                    y = sl_model.predict(x)
            
            result = prior_util.decode(y[0], segment_threshold, link_threshold)
            
            img1 = np.copy(img)
            img2 = np.zeros_like(img)
            

            rboxes = result[:,:5]
            
            if len(rboxes) > 0:
                bh = rboxes[:,3]
                rboxes[:,2] += bh * 0.1
                rboxes[:,3] += bh * 0.2
                
                boxes = np.asarray([rbox_to_polygon(r) for r in rboxes])
                boxes = np.flip(boxes, axis=1)
                boxes = np.reshape(boxes, (-1, 8))
                
                boxes_mask = np.array([not (np.any(b < 0-10) or np.any(b > 512+10)) for b in boxes]) # box inside image
                
                boxes = boxes[boxes_mask]
                rboxes = rboxes[boxes_mask]
                
                if len(boxes) == 0:
                    boxes = np.empty((0,8))
                
                boxes = np.clip(boxes/512, 0, 1)
                #boxes=boxes*1.2
                #for box in boxes:
                #    xs=box[0::2]
                #    ys=box[1::2]
                    #points=np.array([xs,ys]).reshape((4,2))
                    #factor=0.3
                    #p1=points[0]+(points[1]-points[0])*(1+factor)
                    #p0 = points[0] + (points[1] - points[0]) * (1 - factor)
                    #p2 = points[2] + (points[3] - points[2]) * (1 + factor)
                    #p3 = points[2] + (points[3] - points[2]) * (1 - factor)
                    #boxess.append(np.reshape([p0,p1,p2,p3],(-1,8)))
                #    z1=np.polyfit((box[0],box[2]),(box[1],box[3]),1)
                #    z2=np.polyfit((box[4],box[6]),(box[5],box[7]),1)
                #    boxess.append(1.2*box)
                #    print(z1)
                #    print(z2)
                #boxes=boxess.copy()
                boxes_ext=extend_polygons(1.2,boxes.copy())
                boxes=boxes_ext.copy()
                polyss=joiner(boxes)
                boxess=[]
                for poly in polyss:
                    bb=np.array(poly).reshape(1,8)
                    bbx=bb[0][0::2].copy()
                    bby=bb[0][1::2].copy()
                    indexx=np.argsort(bbx)
                    roll=indexx[0]
                    if(bby[indexx[0]]>bby[indexx[1]]):roll=indexx[1]
                    bb=np.roll(bb.copy(),roll*2)
                    boxess.append(bb)
                boxess=np.array(boxess).reshape(-1,8)
                boxes=boxess.copy()

                words,centersx,centersy = crop_words(img, boxes, input_height, width=input_width, grayscale=True)
                words = np.asarray([w.transpose(1,0,2) for w in words])
                Z=[x for _, x in sorted(zip(centersy, words))]

                if len(words) > 0:
                    with crnn_graph.as_default():
                        with crnn_session.as_default():
                            res_crnn = crnn_model.predict(words)
                

                words=Z.copy()
                for i in range(len(words)):
                    idxs = np.argmax(res_crnn[i], axis=1)
                    confs = res_crnn[i][range(len(idxs)),idxs]
                    non_blank_mask = idxs != len(alphabet)-1
                    
                    if np.any(non_blank_mask):
                        mean_conf = np.mean(confs[non_blank_mask])
                        chars = [alphabet[c] for c in idxs]
                        res_str = decode(chars)
                        
                        # filter based on recognition threshold
                        #if mean_conf > 0.7-0.4*np.exp(-0.1*np.sum(non_blank_mask)):
                        if mean_conf > 0.5:
                            b = boxes[i].reshape((-1,1,2)) * [img.shape[0], img.shape[1]]
                            b = np.asarray(np.round(b), dtype=np.int32)
                            cv2.polylines(img1, [b], True, (0,0,255))
                            res_str=res_str.upper()
                            #print("Y location: {0}".format(boxes))
                            if(res_str.count('-')==2):
                                print("Nacimiento en fecha: {0}".format(res_str))
                            elif(res_str.isdecimal() and len(res_str)>5):
                                print("Cedula de identidad es: {0}".format(res_str))
                            else:
                                print("Extra data: {0}".format(res_str))

                            #cv2.imwrite('croped_word_%03i.png' % (i), words[i])
                            #print(res_str)
                        else:
                            #print('drop %5.3f %s' % (mean_conf, res_str))
                            pass
            
            # draw fps
            cv2.rectangle(img1, (0,0), (50, 17), (255,255,255), -1)
            #cv2.putText(img1, fps, (3,10), cv2.FONT_HERSHEY_SIMPLEX, 0.35, (0,0,0), 1)
            
            img = np.concatenate((image, img1.copy()), axis=1)
            cv2.imshow("Resultados ", img)

            #cv2.imshow("masked",maskc)
            if cv2.waitKey(3000) & 0xFF == ord('q'):
                break

# ffmpeg -y -i sl_end2end_record.avi -c:v libx264 -b:v 2400k -preset slow -movflags +faststart -pix_fmt yuv420p sl_end2end_record.mp4
